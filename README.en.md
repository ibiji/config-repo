# config-repo

#### Description
ibiji - mango-config-repo

#### Instructions

1.  配置pom
```xml
服务端
<!-- https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-config-server -->
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-config-server</artifactId>
	<version>${app-project.version}</version>
</dependency>

客户端
<!-- https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-config -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-config</artifactId>
    <version>${app-project.version}</version>
</dependency>
```
2.  服务端主类
```
@EnableConfigServer
```